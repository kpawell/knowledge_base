Типы данных:
 - целочисленные (целые числа)
 - вещественные (десятичные дроби)
 - логические (boolean): True/False
 - символьные (Char)
 - строковые (String)

 Ключи в БД
 - первичный (primary key) - уникальное значение, точно идентифицирующее каждую запись
 - внешний (foreign key) - может быть не уникальным в рамках одной таблицы, но он позволяет связывать таблицы друг с другом

 Базовый запрос

`SELECT` <значение/функция, имена КОЛОНОК> что возвращаем
`FROM` <значение, имя ТАБЛИЦЫ>
`WHERE` <условие>

`select username from USERS
`select username from USERS where username = "Julik"
`select * from USERS

Операторы AND, OR, LIKE, IN (), BETWEEN x AND y
`select username from USERS where username = "Julik" AND id = 8
`select username from USERS where username LIKE "Julik%"

`"Julik%"` - начинается с Julik
`"Julik__"` - начинается с Julik и только два любых символа после

`WHERE id IN (1, 4, 6, 18)
`WHERE id BETWEEN 5 AND 15

`ORDER BY` - сортировка

`WHERE id > 7
`ORDER BY username` (всегда после WHERE)

`ORDER BY username desc` (по убыванию) или `asc` (по возрастанию - можно не писать, по умолчанию)

`LIMIT x, y` - x -сколько пропустить, y - сколько показать после пропущенных
`LIMIT 5` - просто показать первые 5

#### Функции в SQL

некоторые:
`now, current_date, count, min, max, avg, sum, date_sub(date, INTERVAL кол-во ед.изм.)
`select count(*) from USERS
`select TOPIC_STARTER, count(*)
`from TOPIC
`where TOPIC_STARTER = 7
`select now()
`select date_sub (now(), interval 10 day)

ГРУППИРОВКА

Group by

`select user_created, count(post_content) from POST group by user_created

Having - условие отбора после группировки

`select user_created, count(post_content) from POST group by user_created having user_created in (7, 49, 5)

Сначала WHERE с условием выборки,
потом GROUP BY группировка по полю и после HAVING с условием отбора после группировки

Итого порядок

SELECT [DISTINCT] <список полей> или * <функции>
[FROM <список таблиц>]
[WHERE <условие отбора>]
[GROUP BY <список полей для группировки> [ASC | DESC]]
[HAVING <условие группировки>]
[ORDER BY <список полей для сортировки> [ASC | DESC]]
[LIMIT]

JOINS

```sql
select *
from USERS
inner join TOPIC
on TOPIC.topic_starter = USERS.id
```
Inner join выводит только те значения, которые есть в обоих таблицах
Можно просто join (по умолчанию означает inner)

LEFT JOIN - берутся все данные из левой таблицы и добавятся значения из правой. Для тех данных из левой таблицы, которых нет в правой - заполнится нулями.

RIGHT JOIN - с точностью до наоборот. Всем значениям из правой таблицы выбираются значения из левой. заполняется Null, если значий для правой таблицы в левой нет.

Если выбираем значения NULL пишем is: where USER.id is NULL
USER.id is not NULL если не нужны

FULL OUTER JOIN полностью две таблицы
где нет соответствия значений в таблицах будет заполнено null

Зачем знать SQL
- локализация ошибок
- тестирование бекэнда (нет UI)
- тестирование сложных отчётов (того, чего нет в UI)
- тестирование сложных фич

Где тренироваться:
www.sql-ex.ru/
www.w3schools.com/sql/

В принципе, можно объединить таблицы и без JOIN, просто перчеслив таблицы и добавив условие WHERE:
```SQL
select USERS.USERNAME, TOPIC.TITLE
from USERS, TOPIC
where USERS.ID = TOPIC.TOPIC_STARTER
```