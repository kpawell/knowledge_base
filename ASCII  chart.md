#диаграмма 

[ASCII  chart](https://asciiflow.com/)
[iodrow](https://www.iodraw.com/ru/textflow#/) то же, что и выше, только на русском

<div style="line-height:120%; "><code><pre>
┌──────┐
│Привет│
└──────┘
</pre></code></div>

<div style="line-height:120%;"><code><pre>
┌──────┐
│Привет│
└─┬──▲─┘
┌─▼──┴─┐
│ Пока │
└──────┘
</pre></code></div>

<div style="line-height:120%"><code><pre>
┌──────┐
│Привет│
└─┬──▲─┘
  │  │
┌─▼──┴─┐
│ Пока │
└──────┘
</pre></code></div>

