1. Subject - что мы хотим видеть (чем детальнее, тем лучше результат). Кто/что на картинке, в какой позе, как одет, что делает
2. Medium - illustration, oil painting, 3D rendering, photography
3. Style - стиль (realism, surrealism, impressionist, pop art , hyperrealistic, fantasy, dark art)
4. Art-sharing website
5. Resolution - highly detailed, sharp focus
6. Additional details
7. Color - Вы можете управлять общим цветовым оттенком изображения, добавляя цветовые ключевые слова. Указанные вами цвета могут проявиться как тон или в объектах (iridescent gold)
8. Lighting - Ключевые слова по освещению могут оказать огромное влияние на внешний вид изображения

```
amateur woman, pastel colour, ultimate quality, masterpiece, ultra high res, photorealistic, detailed face, detailed eyes, ornamental clother, dramatic lighting, detailed Shot on Hasseblad X1D-50c with Lens Hasselblad XCD-45P, portrait
```

```
amateur woman, (brown eyes), portrait photography, close-up, (natural soft colors), focused eyes, ornamental clothes, cinematic shot, incredibly detailed, super detailed face, incredibly realistic, professional lighting, lightroom, cinematography, hyper realism, 8K movie quality
```

```
brown color of eyes, nacklace, 85 mm 1.2f lens, smiling queen of Balzac's age sitting in the tropical park of flowers, portrait photography, close-up, (natural soft colors), focused eyes, ornamental clothes, cinematic shot, incredibly detailed, super detailed face, incredibly realistic, professional lighting, lightroom, cinematography, hyper realism, 8K movie quality

neg:
(dimple on the chin), (lowres, low quality, worst quality:1.2), (text:1.2), watermark, (frame:1.2), deformed, ugly, deformed eyes, blur, out of focus, blurry, deformed cat, deformed, photo, anthropomorphic cat, monochrome, pet collar, gun, weapon, 3d, drones, drone, buildings in background
```

dynamic angle, (detailed focus:1. 2), dramatic lights, (masterpiece:1. 2), 32k UHD resolution, (best quality:1. 2), RAW, vibrant, (realistic photo:1. 2), (professional photography:1. 2), (highly details:1. 2), depth of field, male