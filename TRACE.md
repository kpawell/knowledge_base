**HTTP Метод `TRACE`** выполняет проверку обратной связи по пути к целевому ресурсу, предоставляя полезный механизм отладки.

Конечный получатель запроса должен отразить полученное сообщение, исключая некоторые поля описанные ниже, назад клиенту как тело сообщения с ответом 200 (`OK`) с заголовком [`Content-Type`](https://developer.mozilla.org/ru/docs/Web/HTTP/Headers/Content-Type) `message/http`. Конечный получатель это либо исходный сервер, либо первый сервер получивший значение [`Max-Forwards` (en-US)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Max-Forwards "Currently only available in English (US)") в запросе.

|   |   |
|---|---|
|Запрос имеет тело|Нет|
|Успешный ответ имеет тело|Нет|
|[Безопасный](https://developer.mozilla.org/ru/docs/Glossary/Safe)|Нет|
|[Идемпотентный](https://developer.mozilla.org/ru/docs/Glossary/Idempotent)|Да|
|[Кешируемый](https://developer.mozilla.org/ru/docs/Glossary/Cacheable)|Нет|
|Допускается в [HTML-формах](https://developer.mozilla.org/ru/docs/Learn/Forms)|Нет|

## Синтаксис

`TRACE /index.html`