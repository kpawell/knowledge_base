**Метод запроса HTTP `PUT`** создаёт новый ресурс или заменяет представление целевого ресурса данными, представленными в теле запроса.

Разница между `PUT` и [`POST`](POST) в том, что `PUT` является идемпотентным, т.е. единичный и множественные вызовы этого метода, с идентичным набором данных, будут иметь тот же результат выполнения (без сторонних эффектов), в случае с `POST`, множественный вызов с идентичным набором данных может повлечь за собой сторонние эффекты.

|   |   |
|---|---|
|Запрос имеет тело|Да|
|Успешный ответ имеет тело|Нет|
|[Безопасный](https://developer.mozilla.org/ru/docs/Glossary/Safe)|Нет|
|[Идемпотентный](https://developer.mozilla.org/ru/docs/Glossary/Idempotent)|Да|
|[Кешируемый](https://developer.mozilla.org/ru/docs/Glossary/Cacheable)|Нет|
|Допускается в [HTML-формах](https://developer.mozilla.org/ru/docs/Learn/Forms)|Нет|

## Синтаксис

`PUT /new.html HTTP/1.1`