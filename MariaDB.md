Запустить
```
sudo systemctl start mariadb
```
остановить
```
sudo systemctl stop mariadb
```
посмотреть статус
```
sudo systemctl status mariadb
```
зайти на сервер под рутом
```
mysql -u root -p
```
пароль как в системе

команды:
Заглавными буквами
```
CREATE DATABASE 'yourDB';
SHOW DATABASES;
```
Создать нового пользователя (user1 и password1 заменить)
```
CREATE USER 'user1'@localhost IDENTIFIED BY 'password1';
```
Посмотреть пользователей
```
SELECT User FROM mysql.user;
```
Наделить привилегиями
```
GRANT ALL PRIVILEGES ON *.* TO 'user1'@localhost IDENTIFIED BY 'password1';
```
или только на конкретную базу
```
GRANT ALL PRIVILEGES ON 'yourDB'.* TO 'user1'@localhost;
```
важно обновить привилегии после наделения
```
FLUSH PRIVILEGES;
```
Посмотреть привилегии
```
SHOW GRANTS FOR 'user1'@localhost;
```
Удалить пользователя
```
DROP USER 'user1'@localhost;
```