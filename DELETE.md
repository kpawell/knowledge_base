

**Метод запроса HTTP `DELETE`** удаляет указанный ресурс.

|   |   |
|---|---|
|Запрос имеет тело|Может|
|Успешный ответ имеет тело|Может|
|[Безопасный](https://developer.mozilla.org/ru/docs/Glossary/Safe)|Нет|
|[Идемпотентный](https://developer.mozilla.org/ru/docs/Glossary/Idempotent)|Да|
|[Кешируемый](https://developer.mozilla.org/ru/docs/Glossary/Cacheable)|Нет|
|Допускается в [HTML-формах](https://developer.mozilla.org/ru/docs/Learn/HTML/Forms)|Нет|

## Синтаксис

DELETE /file.html HTTP/1.1