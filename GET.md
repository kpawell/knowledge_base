
**HTTP-метод `GET`** запрашивает представление указанного ресурса. `GET`-запросы должны только получать данные.

|   |   |
|---|---|
|Запрос имеет тело|Нет|
|Успешный ответ имеет тело|Да|
|[Безопасный](https://developer.mozilla.org/ru/docs/Glossary/Safe)|Да|
|[Идемпотентный](https://developer.mozilla.org/ru/docs/Glossary/Idempotent)|Да|
|[Кешируемый](https://developer.mozilla.org/ru/docs/Glossary/Cacheable)|Да|
|Допускается в [HTML-формах](https://developer.mozilla.org/ru/docs/Learn/Forms)|Да|

## Синтаксис

`GET /index.html`