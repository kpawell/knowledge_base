Установить Charmed MongoDB через snap.

Запустить:

```Shell
sudo snap start charmed-mongodb.mongod
```

Остановить:

```Shell
sudo snap stop charmed-mongodb.mongod
```

Посмотреть логи:

```shell
sudo snap logs charmed-mongodb.mongod
```

Зайти в mongodb через терминал:

```shell 
charmed-mongodb.mongo
```

<u>Настройки по умолчанию:</u>

mongodb://127.0.0.1:27017
