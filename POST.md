**HTTP-метод `POST`** предназначен для отправки данных на сервер. Тип тела запроса указывается в заголовке [`Content-Type`](https://developer.mozilla.org/ru/docs/Web/HTTP/Headers/Content-Type).

Разница между [`PUT`](https://developer.mozilla.org/ru/docs/Web/HTTP/Methods/PUT) и `POST` состоит в том, что `PUT` является идемпотентным: повторное его применение даёт тот же результат, что и при первом применении (то есть у метода нет _побочных эффектов_), тогда как повторный вызов одного и того же метода `POST` может иметь такие эффекты, как например, оформление одного и того же заказа несколько раз.

Запрос `POST` обычно отправляется через форму HTML и приводит к изменению на сервере. В этом случае тип содержимого выбирается путём размещения соответствующей строки в атрибуте [`enctype`](https://developer.mozilla.org/ru/docs/Web/HTML/Element/form#enctype) элемента [`<form>`](https://developer.mozilla.org/ru/docs/Web/HTML/Element/form) или [`formenctype`](https://developer.mozilla.org/ru/docs/Web/HTML/Element/input#formenctype) атрибута элементов [`<input>`](https://developer.mozilla.org/ru/docs/Web/HTML/Element/input) или [`<button>`](https://developer.mozilla.org/ru/docs/Web/HTML/Element/button):

- `application/x-www-form-urlencoded`: значения кодируются в кортежах с ключом, разделённых символом `'&'`, с `'='` между ключом и значением. Не буквенно-цифровые символы - percent encoded: это причина, по которой этот тип не подходит для использования с двоичными данными (вместо этого используйте `multipart/form-data`)
- `multipart/form-data`: каждое значение посылается как блок данных ("body part"), с заданными пользовательским клиентом разделителем ("boundary"), разделяющим каждую часть. Эти ключи даются в заголовки `Content-Disposition` каждой части
- `text/plain`

Когда запрос `POST` отправляется с помощью метода, отличного от HTML-формы, — например, через [`XMLHttpRequest`](https://developer.mozilla.org/ru/docs/Web/API/XMLHttpRequest) — тело может принимать любой тип. Как описано в спецификации HTTP 1.1, `POST` предназначен для обеспечения единообразного метода для покрытия следующих функций:

- Аннотация существующих ресурсов
- Публикация сообщения на доске объявлений, в новостной группе, в списке рассылки или в аналогичной группе статей;
- Добавление нового пользователя посредством модальности регистрации;
- Предоставление блока данных, например, результата отправки формы, процессу обработки данных;
- Расширение базы данных с помощью операции добавления.

| Запрос имеет тело | Да |
| ---- | ---- |
| Успешный ответ имеет тело | Да |
| [Безопасный](https://developer.mozilla.org/ru/docs/Glossary/Safe) | Нет |
| [Идемпотентный](https://developer.mozilla.org/ru/docs/Glossary/Idempotent) | Нет |
| [Кешируемый](https://developer.mozilla.org/ru/docs/Glossary/Cacheable) | Только если включена информация о свежести сообщения |
| Допускается в [HTML-формах](https://developer.mozilla.org/ru/docs/Learn/Forms) | Да |

## Синтаксис

`POST /index.html`