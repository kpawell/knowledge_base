**HTTP-метод** `OPTIONS` используется для описания параметров соединения с целевым ресурсом. Клиент может указать особый URL для обработки метода OPTIONS, или * (звёздочку) чтобы указать весь сервер целиком.

|   |   |
|---|---|
|Запрос имеет тело|Нет|
|Успешный ответ имеет тело|Да|
|[Безопасный](https://developer.mozilla.org/ru/docs/Glossary/Safe)|Да|
|[Идемпотентный](https://developer.mozilla.org/ru/docs/Glossary/Idempotent)|Да|
|[Кешируемый](https://developer.mozilla.org/ru/docs/Glossary/Cacheable)|Нет|
|Допускается в [HTML-формах](https://developer.mozilla.org/ru/docs/Learn/Forms)|Нет|

## Синтаксис

```http
OPTIONS /index.html HTTP/1.1
OPTIONS * HTTP/1.1
```

## Примеры
### Определение разрешённых сервером методов запроса

Для того, чтобы узнать какие методы запросов поддерживаются сервером, можно воспользоваться curl направить OPTIONS запрос:

`curl -X OPTIONS http://example.org -i`

Ответ на запрос содержит [`Allow` (en-US)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Allow "Currently only available in English (US)") заголовок с поддерживаемыми методами:

```http
HTTP/1.1 200 OK
Allow: OPTIONS, GET, HEAD, POST
Cache-Control: max-age=604800
Date: Thu, 13 Oct 2016 11:45:00 GMT
Expires: Thu, 20 Oct 2016 11:45:00 GMT
Server: EOS (lax004/2813)
x-ec-custom-error: 1
Content-Length: 0
```