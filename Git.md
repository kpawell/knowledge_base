#тестирование/git
##### Command line instructions

You can also upload existing files from your computer using the instructions below.

##### Глобальные настройки

```
git config --global user.name "Pavel Kuznetsov"
git config --global user.email "1435326@mail.ru"
```

##### Создать новый репозиторий

```
git clone git@gitlab.com:kpawell/knowledge_base.git
cd knowledge_base
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
```

##### Выгрузить существующую папку

```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:kpawell/knowledge_base.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

##### Push an existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:kpawell/knowledge_base.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```

По умолчанию программа _git_ при выводе путей (команды типа `state`, `ls-files`, `diff` и т.п.) символы с кодом больше `0x80` заменяет их восьмиричными кодами (например, `\320\272` для символа «к»).

для отключения такого преобразования только в текущем хранилище выполните:

```
$ git config core.quotepath false
```

для глобального отключения добавьте опцию `--global`:

```
$ git config --global core.quotepath false
```
