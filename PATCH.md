**Метод запроса HTTP `PATCH`** частично изменяет ресурс.

В какой-то степени `PATCH` можно назвать аналогом действия «обновить» из [CRUD (en-US)](https://developer.mozilla.org/en-US/docs/Glossary/CRUD "Currently only available in English (US)") (однако не следует путать HTTP и [CRUD (en-US)](https://developer.mozilla.org/en-US/docs/Glossary/CRUD "Currently only available in English (US)") — это две разные вещи).

Запрос `PATCH` является набором инструкций о том, как изменить ресурс. В отличие от [`PUT`](PUT), который полностью заменяет ресурс.

`PATCH` может как быть идемпотентным, так и не быть, в отличие от [`PUT`](PUT), который всегда идемпотентен. Операция считается идемпотентной, если её многократное выполнение приводит к тому же результату, что и однократное. Например, если автоинкрементное поле является важной частью ресурса, то [`PUT`](PUT) перезапишет его (т.к. он перезаписывает всё), но `PATCH` может и не перезаписать.

`PATCH` (как и [`POST`](POST)) _может_ иметь побочные эффекты.

Чтобы обозначить, что сервер поддерживает `PATCH`, можно добавить этот метод в список заголовков ответа [`Allow` (en-US)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Allow "Currently only available in English (US)") или [`Access-Control-Allow-Methods`](https://developer.mozilla.org/ru/docs/Web/HTTP/Headers/Access-Control-Allow-Methods) (для [CORS](https://developer.mozilla.org/ru/docs/Web/HTTP/CORS)).

Другим (неявным) индикатором, что метод `PATCH` разрешён, является наличие заголовка [`Accept-Patch`](https://developer.mozilla.org/ru/docs/Web/HTTP/Headers/Accept-Patch), который описывает, какой формат изменения документа принимает сервер.

|   |   |
|---|---|
|Запрос имеет тело|Да|
|Успешный ответ имеет тело|Может|
|[Безопасный](https://developer.mozilla.org/ru/docs/Glossary/Safe)|Нет|
|[Идемпотентный](https://developer.mozilla.org/ru/docs/Glossary/Idempotent)|Нет|
|[Кешируемый](https://developer.mozilla.org/ru/docs/Glossary/Cacheable)|Только если включена информация о дате последнего изменения|
|Допускается в [HTML-формах](https://developer.mozilla.org/ru/docs/Learn/Forms)|Нет|

## Синтаксис

PATCH /file.txt HTTP/1.1

## Пример

### Запрос

HTTPCopy to Clipboard

```
PATCH /file.txt HTTP/1.1
Host: www.example.com
Content-Type: application/example
If-Match: "e0023aa4e"
Content-Length: 100

[описание изменений]
```

### Ответ

Успешный ответ указывается с помощью любого кода ответа серии [2xx](https://httpwg.org/specs/rfc9110.html#status.2xx).

В следующем примере используется код ответа [`204`](https://developer.mozilla.org/ru/docs/Web/HTTP/Status/204), поскольку ответ не содержит тела сообщения. Если требуется передать тело, то используется код [`200`](https://developer.mozilla.org/ru/docs/Web/HTTP/Status/200).

```
HTTP/1.1 204 No Content
Content-Location: /file.txt
ETag: "e0023aa4f"
```